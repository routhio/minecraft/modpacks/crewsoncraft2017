FROM registry.gitlab.com/routhio/minecraft/tools/packinit:0.3.3

RUN mkdir -p /dist/crewsoncraft2017
ADD packmaker.yml   /dist/crewsoncraft2017/packmaker.yml
ADD packmaker.lock  /dist/crewsoncraft2017/packmaker.lock
COPY src            /dist/crewsoncraft2017/src

ENV PRIMARY_PACK=/dist/crewsoncraft2017
