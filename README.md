# CREWSONCRAFT 2017

Launch the basic server image (with local log output):
```bash
docker run -it -v cache:/cache -v server:/server -P registry.routh.io/minecraft/modpacks/crewsoncraft2017:latest
```

The server does not come with an Mojang eula.txt license file, but will generate one when you first launch it.
You can stop the server, edit the file (found in the server volume), and relaunch to get the server started.
