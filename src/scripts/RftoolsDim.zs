import mods.jei.JEI.removeAndHide as rh;

print(">>> loading RftoolsDim.zs"); 

# Pulverize dimensional shards

mods.thermalexpansion.Pulverizer.addRecipe(<rftools:dimensional_shard> * 2, <rftools:dimensional_shard_ore>, 4000, <rftools:dimensional_shard>, 20);
mods.thermalexpansion.Pulverizer.addRecipe(<rftools:dimensional_shard> * 2, <rftools:dimensional_shard_ore:1>, 4000, <rftools:dimensional_shard>, 20);
mods.thermalexpansion.Pulverizer.addRecipe(<rftools:dimensional_shard> * 2, <rftools:dimensional_shard_ore:2>, 4000, <rftools:dimensional_shard>, 20);


print("<<< finished RftoolsDim.zs"); 
