import mods.jei.JEI.removeAndHide as rh;

print(">>> loading Mekanism.zs"); 
	
# Removing unused ores & Walkietalkie
rh(<mekanism:oreblock:1>);
rh(<mekanism:oreblock:2>);
rh(<mekanism:walkietalkie>);

print("<<< finished Mekanism.zs");
