import mods.jei.JEI.removeAndHide as rh;

print(">>> loading RftoolsDim.zs"); 

# Flux can be created in a thermalexpansion fluid transposer, using lava and redstone

mods.thermalexpansion.Transposer.addFillRecipe(<fluxnetworks:flux>, <minecraft:redstone>, <liquid:lava> * 100, 800);

print(">>> finished RftoolsDim.zs"); 
