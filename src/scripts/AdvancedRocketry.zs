#
# I done messed up and removed any way of getting titanium in the overworld except via chickens.
# So lets add a nuclearcraft allow furnace recipe to create it. Its expensive, but should get the
# job done until the player gets to space and can find rutile ore on other planets.

print(">>> loading AdvancedRocketry.zs");

# NuclearCraft recipes for titanium
mods.nuclearcraft.alloy_furnace.addRecipe(<ore:ingotSilver> * 3, <ore:ingotEnderium>, <ore:ingotTitanium>);
mods.nuclearcraft.alloy_furnace.addRecipe(<ore:blockSilver>, <ore:ingotEnderium> * 3, <ore:ingotTitanium> * 3);

# ThermalExpansion recipe(s) for titanium
mods.thermalexpansion.InductionSmelter.addRecipe(<libvulpes:productingot:7>, <thermalfoundation:material:130> * 3, <thermalfoundation:material:167>, 9600);
mods.thermalexpansion.InductionSmelter.addRecipe(<libvulpes:productingot:7> * 3, <thermalfoundation:storage:2>, <thermalfoundation:material:167> * 3, 28800);

print("<<< finished AdvancedRocketry.zs");
